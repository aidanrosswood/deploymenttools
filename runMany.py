import argparse
import os

import utils

DEFAULT_STARTING_PORT = 40001

def get_hostname_args(idx, ports):
    reordered_ports = ports[idx:] + ports[:idx]
    return ' '.join(reordered_ports)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run a distributed system locally with a ton of nodes.')
    parser.add_argument('jar', type=str, help='Path to jar file to run')
    parser.add_argument('logLevel', type=str, help='Log level: ERR|WARN|INFO|DEBUG')
    parser.add_argument('metric', type=str, help='Metrics on: TRUE|FALSE')
    parser.add_argument('nodes', type=int, help='The number of nodes to use')
    parser.add_argument('--log-latency', help='Start the latency logger thread on the servers', action='store_true')
    args = parser.parse_args()
    ports = list(range(DEFAULT_STARTING_PORT, DEFAULT_STARTING_PORT + args.nodes*3, 3))
    hostnames = [f'127.0.0.1:{port}' for port in ports]
    local_servers_path = utils.get_project_path('local_servers.txt')
    with open(local_servers_path, 'w') as f:
        for h in hostnames:
            f.write(h)
            f.write('\n')

    other_args = '--log-latency' if args.log_latency else ''
    commands = [
        f'java -Xmx64m -XX:+UseSerialGC -jar {args.jar} {args.logLevel} {args.metric} {hostname} {local_servers_path} {other_args}'
        for hostname in hostnames
    ]
    output_dir = utils.get_project_path(f'.output')
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    files = [open(os.path.join(output_dir, f'{port}.txt'), 'w') for port in ports]
    utils.parallelize_commands(commands, files=files)
