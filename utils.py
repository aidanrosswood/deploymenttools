import os
import subprocess
import json
import collections

PROJECT_ROOT = os.path.dirname(os.path.realpath(__file__))


def collect_hostnames(filename):
	with open(filename, 'r') as f:
		return [host.strip() for host in f.readlines()]


def dump_hostnames(filename, hosts):
    with open(filename, 'w') as f:
        for h in sorted(hosts):
            f.write(h + '\n')

def get_project_path(filename):
    return os.path.join(PROJECT_ROOT, filename)


def get_output_dir():
    d = get_project_path('.output')
    if not os.path.exists(d):
        os.mkdir(d)
    return d

def print_pssh_output(output):
    for node, result in output.items():
        for line in result.stdout:
            print(f'[{node}] {line}')
        for line in result.stderr:
            print(f'ERROR: [{node}] {line}')


def parallelize_commands(commands, files=None):
    if files is None:
        files = [subprocess.PIPE for c in commands]
    procs = [
        subprocess.Popen(cmd, stdout=f, stderr=f, shell=True)
        for cmd, f in zip(commands, files)
    ]

    successes, fails = {}, {}

    for i, proc in enumerate(procs):
        proc.wait()
        ret = proc.poll()
        stdout, stderr = proc.communicate()
        if ret != 0:
            fails[i] = stderr.decode('utf-8')
        else:
            successes[i] = stdout.decode('utf-8')

    return successes, fails


def run_commands(hosts, commands, user, key, tail=False, timeout=10):
    if tail:
        output_dir = get_output_dir()
        ssh_commands = [f"""ssh -l {user} -i {key} -o ConnectTimeout={timeout} -o 'StrictHostKeyChecking no' -o BatchMode=yes {host} '{cmd}' > {output_dir}/{host}.txt""" for host, cmd in zip(hosts, commands)]
    else:
        ssh_commands = [f"""ssh -l {user} -i {key} -o ConnectTimeout={timeout} -o 'StrictHostKeyChecking no' -o BatchMode=yes {host} '{cmd}'""" for host, cmd in zip(hosts, commands)]
    procs = {
        host: subprocess.Popen(ssh_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        for host, ssh_cmd in zip(hosts, ssh_commands)
    }

    successes, fails = {}, {}

    for host, proc in procs.items():
        proc.wait()
        ret = proc.poll()
        stdout, stderr = proc.communicate()
        if ret != 0:
            fails[host] = stderr.decode('utf-8')
        else:
            successes[host] = stdout.decode('utf-8')

    return successes, fails

def run_command(hosts, command, user, key, tail=False, timeout=10):
    return run_commands(hosts, [command]*len(hosts), user, key, tail=tail,timeout=timeout)

def print_command_result(successes, fails):
    if successes:
        print("Sucess:")
        print(successes)
    if fails:
        print("Fail:")
        print(fails)


def parse_latency_line(line):
    # Parse a line like this
    # [LATENCY_LOGGER] 127.0.0.1:40014 -> 127.0.0.1:40003 | {"avg_latency": 2.600000, "loss_factor": 0.000000}
    left, right = line.strip('[LATENCY_LOGGER] ').split(' | ')
    data = json.loads(right)
    from_node, to_node = left.strip().split(' -> ')
    return from_node, to_node, data


def parse_latency_logfile(log_filename):
    graph = collections.defaultdict(dict)
    for line in open(log_filename).readlines():
        if '[LATENCY_LOGGER] COMPLETED LOGGING' in line:
            break
        if not line.startswith('[LATENCY_LOGGER]'):
            continue
        from_node, to_node, data = parse_latency_line(line)

        graph[from_node][to_node] = data
    
    return dict(graph)


def get_latency_graph(list_of_logfiles):
    graph = {}
    for fname in list_of_logfiles:
        subgraph = parse_latency_logfile(fname)
        graph.update(subgraph)
    return graph


if __name__ == '__main__':
    graph = get_latency_graph([f'.output/{port}.txt' for port in range(40001, 40021)])
    
    import pprint
    pprint.pprint(graph)
