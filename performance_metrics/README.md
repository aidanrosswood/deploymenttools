# CPEN431 NODE PERFORMANCE RANKING TOOL #



### About the Tool ###

To use this tools you **must have python version of 3.6 or above** installed in your command line and run
```
pip install -r requirements.txt
```

To run the tool, use command 
```
python3 getNodes.py <ranking-criteria>
```
and the list of nodes which be sorted based on the input criteria.
```
<ranking-criteria> = 1, sorting is based on CPU load
<ranking-criteria> = 2, sorting is based on network traffic
<ranking-criteria> = 3, sorting based on free memory space on the node

```
If no criteria is provided, the tool will sort hosts on the list by CPU load

### About the output ###
The best node based on the input criteria appear on top of the file, nodes get worse from top to bottom.

ranking.txt includes detailed metrics whereas ranking_host_name_only displays names of nodes.