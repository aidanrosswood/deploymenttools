import requests
import re
import time
import constant 
import sys
import command
from collections import OrderedDict
from operator import itemgetter  

# index convention 0: memory space
#                  1: cpu load fraction
#                  2: upload traffic
#                  3: download traffic
#                  4: total traffic


if __name__ == '__main__':   
    sortCriteria = 0
    if(len(sys.argv)<2):
        sortCriteria = 1
    else:
       sortCriteria = int(sys.argv[1]) 

    file1 = open('nodes_list.txt', 'r') 
    nodes = []
    hashMapMem = {} 
    hashMapRank={}
    while True: 
        line = file1.readline() 
        if not line: 
            break

        nodes.append(line.strip())
  
    file1.close() 


    i = 0
    while i < len(nodes): 
        try: 
            r = requests.get('http://' +  nodes[i] + '/metrics',timeout=30)
            if r.status_code == 200:
                content = r.text

                freeMemory = re.findall(r"node\_memory\_MemFree\_bytes.+\n",content)
                sciNot = freeMemory[2].split()[1].split('\n')[0]
                memNum = float(sciNot)

                load = re.findall(r"node\_load1.+\n",content)
                loadNum = float(load[2].split()[1].split('\n')[0])
                cpuCount = 0

                while True:
                    if len((re.findall('cpu='+'"'+ str(cpuCount) +'"', content)))!=0:
                        cpuCount = cpuCount + 1        
                    else:
                        break
                print('requesting data from ' + nodes[i])
                eth0_in = re.findall(r"node\_network\_receive\_bytes\_total\{device=\"eth0\"\}.+\n",content)[0]
                sciNot = eth0_in.split()[1].split('\n')[0]
                eth0_in_1 = float(sciNot)

                lo_in = re.findall(r"node\_network\_receive\_bytes\_total\{device=\"lo\"\}.+\n",content)[0]
                sciNot = lo_in.split()[1].split('\n')[0]
                lo_in_1 = float(sciNot)
                
                total_in_1 = lo_in_1 + eth0_in_1 

                eth0_out = re.findall(r"node\_network\_transmit\_bytes\_total\{device=\"eth0\"\}.+\n",content)[0]
                sciNot = eth0_out.split()[1].split('\n')[0]
                eth0_out_1 = float(sciNot)

                lo_out = re.findall(r"node\_network\_transmit\_bytes\_total\{device=\"lo\"\}.+\n",content)[0]
                sciNot = lo_out.split()[1].split('\n')[0]
                lo_out_1 = float(sciNot)

                total_out_1 = eth0_out_1 + lo_out_1

                hashMapMem[nodes[i]]=[memNum,loadNum/cpuCount*100,total_out_1,total_in_1,total_out_1+total_in_1]
                i = i+1
        except:
            continue

    time.sleep(5)

    i = 0
    while i < len(nodes): 
        try:
            r = requests.get('http://' +  nodes[i] + '/metrics')
            if r.status_code == 200:
                content = r.text

                print("Calculating network traffic for " + nodes[i])                    
                eth0_in = re.findall(r"node\_network\_receive\_bytes\_total\{device=\"eth0\"\}.+\n",content)[0]
                sciNot = eth0_in.split()[1].split('\n')[0]
                eth0_in_2 = float(sciNot)

                lo_in = re.findall(r"node\_network\_receive\_bytes\_total\{device=\"lo\"\}.+\n",content)[0]
                sciNot = lo_in.split()[1].split('\n')[0]
                lo_in_2 = float(sciNot)
                
                total_in_2 = eth0_in_2 + lo_in_2

                eth0_out = re.findall(r"node\_network\_transmit\_bytes\_total\{device=\"eth0\"\}.+\n",content)[0]
                sciNot = eth0_out.split()[1].split('\n')[0]
                eth0_out_2 = float(sciNot)

                lo_out = re.findall(r"node\_network\_transmit\_bytes\_total\{device=\"lo\"\}.+\n",content)[0]
                sciNot = lo_out.split()[1].split('\n')[0]
                lo_out_2 = float(sciNot)

                total_out_2 = eth0_out_2 + lo_out_2

                hashMapMem[nodes[i]][constant.UPLOAD_FIELD]=total_out_2/5 - hashMapMem[nodes[i]][constant.UPLOAD_FIELD]/5
                hashMapMem[nodes[i]][constant.DOWNLOAD_FIELD]=total_in_2/5 - hashMapMem[nodes[i]][constant.DOWNLOAD_FIELD]/5
                hashMapMem[nodes[i]][constant.TOTAL_TRAFFIC]=hashMapMem[nodes[i]][constant.UPLOAD_FIELD] + hashMapMem[nodes[i]][constant.DOWNLOAD_FIELD] 
                i = i+1
        except:
            continue
        
    result = {}
    if(sortCriteria==command.BEST_NETWORK):
       result = sorted(hashMapMem.items(), key=lambda r: r[1][constant.TOTAL_TRAFFIC], reverse=False)
    elif(sortCriteria==command.BEST_MEMORY):
        result = sorted(hashMapMem.items(), key=lambda r: r[1][constant.FREE_MEMORY], reverse=True)
    else:
        result = sorted(hashMapMem.items(), key=lambda r: r[1][constant.CPU_LOAD], reverse=False)

    i = 0
    f = open("ranking.txt", "w")
    g = open("ranking_host_name_only.txt","w")
    while i < len(result):
        print(result[i])
        host = result[i][0].split(":")[0]
        metrics = result[i][1]
        free = format(metrics[constant.FREE_MEMORY]/1024/1024,'3f')
        cpuLoad = format(metrics[constant.CPU_LOAD],'3f')
        traffic = format(metrics[constant.TOTAL_TRAFFIC]/1000,'3f')
        f.writelines("Hostname: "+ host +"  " + "Free memory in MiB: " + free + "  " + "CPU load percentage: "+ cpuLoad + "  " + "Instant Network traffic in KB/s: " + traffic +"\n")
        g.writelines(host + "\n")
        i = i+1
    f.close() 