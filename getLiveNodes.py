import argparse

import utils
import local_config

BATCH_SIZE = 40

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='See all live PlanetLab nodes.')
    parser.add_argument('-v', '--verbose', help='Print errors', action='store_true')
    parser.add_argument('-n', '--not-setup', help='Only display nodes that are not setup', action='store_true')
    parser.add_argument('-s', '--servers', help='Path to servers file for servers to check')
    args = parser.parse_args()

    key = local_config.get_planetlab_key()

    if not args.servers: 
        PLC_nodes = utils.collect_hostnames(utils.get_project_path('PLC_nodes.txt'))
        PLE_nodes = utils.collect_hostnames(utils.get_project_path('PLE_nodes.txt'))
        all_nodes = list(set().union(*[PLC_nodes, PLE_nodes]))
    else:
        all_nodes = utils.collect_hostnames(args.servers)
    successes, failures = {}, {}
    for i in range(0, len(all_nodes), BATCH_SIZE):
        if args.verbose:
            print('Running on batch', i//BATCH_SIZE+1, 'of', len(all_nodes)//BATCH_SIZE+1)
        nodes = all_nodes[i:i+BATCH_SIZE]
        bsuccesses, bfailures = utils.run_command(nodes, 'echo test', 'ubc_cpen431_5', key, timeout=5)
        successes.update(bsuccesses)
        failures.update(bfailures)
    if args.verbose:
        if failures:
            fmt_len = max((len(h) for h in failures.keys()))
            for host, stderr in failures.items():
                print(f'ERROR [{host+" "*(fmt_len-len(host))}] {stderr}'.strip())
        print('\nLIVE NODES:')

    setup_servers = utils.collect_hostnames(utils.get_project_path('setup_servers.txt'))

    for host in sorted(successes.keys()):
        if args.not_setup == False or (args.not_setup and host not in setup_servers):
            print(host)
