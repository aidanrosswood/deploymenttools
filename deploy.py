import sys
import argparse
import subprocess
import os

import utils
import local_config
import fileUpload
import setupServer

DEFAULT_PROC_NAME = '431server'
DEFAULT_JAR_PATH = '~/server.jar'
DEFAULT_SERVERS_LIST_PATH = '~/servers_list.txt'
DEFAULT_LOGFILE_PATH = '~/server.log'
DEFAULT_PORT = '41337'
DEFAULT_JAVAAGENT_PORT = '48200'


def stop_cmd():
	parser = argparse.ArgumentParser(description='Start server in multiple nodes.')
	parser.add_argument('--servers', type=str, help='path of the servers list file', required=True)
	args = parser.parse_args(sys.argv[2:])
	key = local_config.get_planetlab_key()
	hosts = utils.collect_hostnames(args.servers)
	stop_all(hosts, key)

def start_cmd():
	parser = argparse.ArgumentParser(description='Start server in multiple nodes.')
	parser.add_argument('--servers', type=str, help='path of the servers list file', required=True)
	parser.add_argument('--log-level', type=str, help='Log level', choices=['ERR', 'DEBUG', 'INFO', 'WARN'], default='ERR')
	parser.add_argument('--metric', type=str, help='metric', choices=['TRUE', 'FALSE'], default='FALSE')
	parser.add_argument('--jar', type=str, help='path to local JAR file to upload')
	args = parser.parse_args(sys.argv[2:])
	key = local_config.get_planetlab_key()
	hosts = utils.collect_hostnames(args.servers)

	print('Setting up hosts on:', '\n    '.join([''] + hosts))

	if args.jar:
		print('Uploading', args.jar)
		fileUpload.upload_file_no_pssh(hosts, key, args.jar, DEFAULT_JAR_PATH)

	start(hosts, key, args.log_level, args.metric)

def stop_all(hosts, key):
	bsuccesses, bfailures = utils.run_command(hosts, f'pkill -f {DEFAULT_PROC_NAME}', 'ubc_cpen431_5', key, timeout=5)
	utils.print_command_result(bsuccesses, bfailures) 


def start(hosts, key, log_level, metric):
	stop_all(hosts, key)
	# TODO add checks to see if command failed by parsing output
	tmp_servers_list_path = os.path.join(utils.get_output_dir(), 'deployed_servers_list.txt')
	utils.dump_hostnames(tmp_servers_list_path, [f'{h}:{DEFAULT_PORT}' for h in hosts])
	fileUpload.upload_file_no_pssh(hosts, key, tmp_servers_list_path, DEFAULT_SERVERS_LIST_PATH)

	javaagent_jar_path = setupServer.get_server_config_path('jmx_prometheus_javaagent-0.12.0.jar')
	javaagent_conf_path = setupServer.get_server_config_path('javaagent_config.yml')
	commands = [
		f'exec -a {DEFAULT_PROC_NAME} java -javaagent:{javaagent_jar_path}={DEFAULT_JAVAAGENT_PORT}:{javaagent_conf_path} -Xmx64m -XX:+UseSerialGC -jar {DEFAULT_JAR_PATH} {log_level} {metric} {addr} {DEFAULT_SERVERS_LIST_PATH} > {DEFAULT_LOGFILE_PATH} &'
		for addr in [f'{h}:{DEFAULT_PORT}' for h in hosts]
	]
	for c, h in zip(commands, hosts):
		print(f'starting server {h} with cmd: \n', c)
	bsuccesses, bfailures = utils.run_commands(hosts, commands, 'ubc_cpen431_5', key, timeout=5)
	utils.print_command_result(bsuccesses, bfailures) 
	tail_log_files(hosts, key)

def tail_log_files(hosts, key):
	command = 'tail -f server.log'
	utils.run_command(hosts, command, 'ubc_cpen431_5', key, tail=True, timeout=5)


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Start/Stop server in multiple nodes or upload an executable.')
	parser.add_argument('action', help='start/stop server or upload a JAR file', choices=["start", "stop"])
	args = parser.parse_args(sys.argv[1:2])
	if args.action == 'start':
		start_cmd()
	else:
		stop_cmd()
