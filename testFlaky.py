import subprocess
import io
import sys
import time

import utils


def output_is_success(stdout, stderr):
    for line in stdout.split('\n'):
        if 'Test Status:' in line:
            return 'TEST_PASSED' in line

    raise Exception('Output did not actually run the test. Did you pass correct args?\nOUTPUT:\n' + stderr)

if __name__ == '__main__':
    testjar = utils.get_project_path('cpen431_closedloop_2020_v1.jar')
    
    N_RUNS = 30

    for i in range(N_RUNS):
        sio = io.StringIO('')
        out = subprocess.run(f'java -jar {testjar} {" ".join(sys.argv[1:])}', shell=True, capture_output=True)
        # print(out)
        stdout, stderr = out.stdout.decode(), out.stderr.decode()
        print(stdout)
        if not output_is_success(stdout, stderr):
            print('FAILURE ON RUN ', i+1)
            exit(1)

        time.sleep(5)
