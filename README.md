# CPEN431DeploymentTools

File upload and deployment scripts to automate starting and stopping scripts.

To use these tools you **must have SSH and SCP** installed in your command line and run
```
pip install -r requirements.txt
```

The first time you run these scripts you will be prompted to enter the paths to your SSH private keys. Example:
```
Authorization required. Do not have key for PlanetLab.
Please enter the path for key "PlanetLab":~/.ssh/plab
Authorization required. Do not have key for Prometheus.
Please enter the path for key "Prometheus":~/.ssh/prometheus
```
You can change these at any time by either deleting the .local_conf directory or changing the files
inside of the .local_conf directory.


## Setting up a new node
To install Java + monitoring software on a node run:
```
python3 setupServer.py -s planetlab.hostname.com
```
Note that running this command will alter `all_servers.txt` so remember to commit
those changes.


## Syncing nodes with Prometheus
To sync nodes with Prometheus so that all nodes upload their metrics run:
```
python3 syncPrometheus.py
```
This should be done after adding/removing a node or starting a new Java service.


## Deployment Workflow
To deploy to a set of nodes, first add the hostnames of your nodes to the ``servers_to_be_setup.txt`` file. Then stop existing services, upload a JAR, start services and resync Prometheus so it can grab on to the newly started JMX processes.

```
python3 deploy.py start --log-level <ERR|WARN|DEBUG|INFO> --metric <TRUE|FALSE> --servers servers.txt --jar path/to/local/jar.jar
python3 syncPrometheus.py
```


## Testing Locally
To run a distributed system with `n` nodes running on localhost on different ports
```
python3 DeploymentTools/runMany.py path/to/server.jar <log level: ERR|WARN|INFO|DEBUG> <metrics on: TRUE|FALSE> n
```
This will output a file `local_servers.txt` so that you can run an evaluation
client against the local system. For example:
```
java -jar a6_2020_eval_tests_v1.jar --servers-list DeploymentTools/local_servers.txt
```
All `n` nodes will write their output to a hidden directory called `.output`
with one output file per node.


## Useful snippets

To setup all servers that are accessible via SSH
```
python3 getLiveNodes.py | xargs -I % python3 setupServer.py %
```

To setup all servers that have never been setup but are accessible via SSH
```
python3 getLiveNodes.py -n | xargs -I % python3 setupServer.py %
```

# CPEN431 PERFORMANCE METRICS TOOL RANKING TOOL #



### About the Tool ###

To use this tools you **must have python version of 3.6 or above** installed in your command line and under the performance_metrics directory run
```
pip install -r requirements.txt
```

To run the tool, under the same directory above, use command 
```
python3 getNodes.py <ranking-criteria>
```
and the list of nodes which be sorted based on the input criteria.
```
<ranking-criteria> = 1, sorting is based on CPU load
<ranking-criteria> = 2, sorting is based on network traffic
<ranking-criteria> = 3, sorting based on free memory space on the node

```
If no criteria is provided, the tool will sort hosts on the list by CPU load

### About the output ###
Output logs appear under the performance_metrics directory each time after getNodes.py runs. The best node based on the input criteria appear on top of the file, nodes get worse from top to bottom.

ranking.txt includes detailed metrics whereas ranking_host_name_only displays names of nodes.